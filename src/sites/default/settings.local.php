<?php


/**
 * @file
 * Local development override configuration feature.
 */

ini_set('memory_limit', '320M');

/**
 * Database settings:
 */
$databases['default']['default'] = array (
  'database' => 'bbh',
  'username' => 'bbh',
  'password' => 'drupal.@dm1n',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

/**
 * Location of the site configuration files.
 */
$config_directories = array();
$config_directories['sync'] = 'sites/default/files/config_8KGsb-8F4eSsJZJwT0KoYtzOvJZ7djO2qFucx4I5Hm7DBYlt9W8C5gjfiKmp9kTaKMItcxrmxw/sync';

/**
 * Settings:
 */

/**
 * The active installation profile.
 */
$settings['install_profile'] = 'standard';

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 */
$settings['hash_salt'] = 'IJ4PO_o1Sf3qcNYHdzEMPGSlBMhap_qL8UoOV-CcFqkItj60FpLKaz8X9IONLQuxZ96wNBZ5AQ';

/**
 * Configuration overrides.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Enable local development services.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Show all error messages, with backtrace information.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Disable the render cache (this includes the page cache).
 */
# $settings['cache']['bins']['render'] = 'cache.backend.null';

/**
 * Disable Dynamic Page Cache.
 */
# $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

/**
 * Allow test modules and themes to be installed.
 */
$settings['extension_discovery_scan_tests'] = TRUE;

/**
 * Enable access to rebuild.php.
 */
$settings['rebuild_access'] = TRUE;

/**
 * Skip file system permissions hardening.
 */
$settings['skip_permissions_hardening'] = TRUE;

/**
 * Assertions.
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();
